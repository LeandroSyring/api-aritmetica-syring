package com.br.itau.controllers;

import com.br.itau.models.Matematica;
import com.br.itau.services.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaService;

    @PostMapping("soma")
    public Integer soma(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        if (numeros.isEmpty() || numeros.size() < 2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "É necessário ao menos 2 numeros para serem somados");
        }
        for(int numero: numeros){
            if(numero < 0) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Fovar informar numeros naturais");
            }
        }
        return matematicaService.soma(numeros);
    }

    @PostMapping("subtracao")
    public Integer subtracao(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        if(numeros.isEmpty() || numeros.size() < 2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "É necessário ao menos 2 numeros para serem subtraídos");
        }
        for(int numero: numeros){
            if(numero < 0) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Fovar informar numeros naturais");
            }
        }
        return matematicaService.subtracao(numeros);
    }

    @PostMapping("multiplicacao")
    public Integer multiplicacao(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        if(numeros.isEmpty() || numeros.size() < 2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "É necessário ao menos 2 numeros para serem multiplicados");
        }
        for(int numero: numeros){
            if(numero < 0) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Fovar informar numeros naturais");
            }
        }
        return matematicaService.multiplicacao(numeros);
    }

    @PostMapping("divisao")
    public Double divisao(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        if(numeros.isEmpty() || numeros.size() < 2 || numeros.size() > 2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Favor informar dois numeros para serem divididos");
        }
        for(int numero: numeros){
            if(numero < 0) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        "Fovar informar numeros naturais");
            }
        }
        return matematicaService.divisao(numeros);
    }
}
