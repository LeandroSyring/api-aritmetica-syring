package com.br.itau.services;

import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class MatematicaService {

    public int soma(List<Integer> numeros){
        int resultado = 0;
        for(int numero: numeros){
            resultado += numero;
        }
        return resultado;
    }

    public int subtracao(List<Integer> numeros){
        int resultado = 0;
        for(int numero: numeros){
            resultado -= numero;
        }
        return resultado;
    }

    public int multiplicacao(List<Integer> numeros){
        int resultado = 1;
        for(int numero: numeros){
            resultado *= numero;
        }
        return resultado;
    }

    public double divisao(List<Integer> numeros){

        double resultado = 0;

        Collections.sort(numeros);

        resultado = numeros.get(1) / numeros.get(0);

        return resultado;

    }
}
